#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  _   _       _  __ _ 
# | | | |     (_)/ _(_)
# | | | |_ __  _| |_ _ 
# | | | | '_ \| |  _| |
# | |_| | | | | | | | |
#  \___/|_| |_|_|_| |_|
#
# created: 01/2022
#
# Author: Frank Baier
#
from .agent_based_api.v1 import *

def parse_homematic(string_table):
  parsed = { }
  for line in string_table:
    key = line[0]
    device = parsed.get(key)
    if key == 'SVC_MSG':
      if device is None:
        device = []
        parsed[key] = device
      device.append(line[1:])
    else:
      if device is None:
        device = { }
        parsed[key] = device
      device[line[1]] = line[2:]
  return parsed


register.agent_section(
    name = "homematic",
    parse_function = parse_homematic,
)



# general ccu check
def discover_homematic_info(section):
  yield Service()


def check_homematic_info(section):
  for line in section:
    data = section[line]
    if isinstance(data, dict) and data.get('DUTY_CYCLE', None) is not None:
      device_type = data.get('TYPE', None)[0]
      device_firmware = data.get('FIRMWARE_VERSION', None)[0]
      device_description = data.get('DESCRIPTION', None)[0]
      device_serial = line

  yield Result(state=State.OK, summary=f'Device-Typ: {device_type}, Serial-No: {device_serial}, Firmware: {device_firmware}, Description: {device_description}')


register.check_plugin(
    name = "homematic_info",
    service_name = "Homematic CCU",
    sections=["homematic"],
    discovery_function = discover_homematic_info,
    check_function = check_homematic_info,
)


# check for homematic battery / unreachable / service messages
def discover_homematic_alarms(section):
  yield Service(item=f'Low Battery devices')
  yield Service(item=f'Unreachable devices')
  yield Service(item=f'Service Messages')


def check_homematic_alarms(item, section):
  messages = section.get('SVC_MSG', None);

  state = 0
  devices = []

  if messages is not None:
    for msg in messages:
      if item == 'Low Battery devices':
        if msg[1] == 'LOWBAT':
          state = 2
          yield Result(state=State.CRIT, summary=f'{msg[0]}')
      elif item == 'Unreachable devices':
        if msg[1] == 'UNREACH':
          state = 2
          yield Result(state=State.CRIT, summary=f'{msg[0]}')
        elif msg[1] == 'STICKY_UNREACH':
          state = max(state, 1)
          yield Result(state=State.CRIT, summary=f'{msg[0]}')
      elif item == 'Service Messages':
        if msg[1] not in [ 'LOWBAT', 'UNREACH', 'STICKY_UNREACH' ]:
          if msg[1] in [ 'CONFIG_PENDING', 'DEVICE_IN_BOOTLOADER', 'UPDATE_PENDING', 'USBH_POWERFAIL' ]:
            state = max(state, 1)
            yield Result(state=State.WARN, summary=f'{msg[0]}: {msg[1]}')
          else:
            state = 2
            yield Result(state=State.CRIT, summary=f'{msg[0]}: {msg[1]}')

  if state == 0:
    yield Result(state=State.OK, summary=f'No issues reported')


register.check_plugin(
    name = "homematic_alarms",
    service_name = "Homematic %s",
    sections=["homematic"],
    discovery_function = discover_homematic_alarms,
    check_function = check_homematic_alarms,
)



# check for climate sensor
def discover_homematic_climate(section):
  for line in section:
    data = section[line]
    if isinstance(data, dict):
      HssType = data.get('HSSTYPE', None)

      if HssType is not None:
        if set(HssType).issubset(['HM-CC-RT-DN', 'HM-TC-IT-WM-W-EU', 'HM-WDS10-TH-O']):
          yield Service(item=line)


def check_homematic_climate(item, params, section):
  (warnlo, critlo) = params['humidity_low']
  (warnhi, crithi) = params['humidity_high']

  for line in section:
    if line == item:
      data = section[line]
      last_change = data.get('TEMPERATURE', data.get('ACTUAL_TEMPERATURE', ['-', '-']))[1]

      if data.get('HUMIDITY', None) is not None:
        humidity = round(float(data.get('HUMIDITY', ['0'])[0]),1)
        yield Metric("humidity", humidity, levels=(warnhi, crithi), boundaries=(0,100))

        if humidity <= critlo or humidity >= crithi:
          yield Result(state = State.CRIT, summary = f'Humidity: {humidity:.1f}%')
        elif humidity <= warnlo or humidity >= warnhi:
          yield Result(state = State.WARN, summary = f'Humidity: {humidity:.1f}%')
        else:
          yield Result(state = State.OK, summary = f'Humidity: {humidity:.1f}%')

      if data.get('TEMPERATURE', data.get('ACTUAL_TEMPERATURE', None)) is not None:
        temperature = round(float(data.get('TEMPERATURE', data.get('ACTUAL_TEMPERATURE', ['0']))[0]),1)
        yield Metric("temperature", temperature)
        yield Result(state = State.OK, summary = f'Temperature: {temperature:.1f}')

      yield Result(state = State.OK, summary = f'last change: {last_change}')


register.check_plugin(
    name = "homematic_climate",
    service_name = "HM-Climate %s",
    sections=["homematic"],
    discovery_function = discover_homematic_climate,
    check_function = check_homematic_climate,
    check_default_parameters={'humidity_low' : (35, 30),'humidity_high': (60, 70)},
    check_ruleset_name="homematic_climate",)



# check for power plugs with energy sensor
def discover_homematic_power(section):
  for line in section:
    data = section[line]
    if isinstance(data, dict):
      HssType = data.get('HSSTYPE', None)

      if HssType is not None:
        if set(HssType).issubset(['HM-ES-PMSw1-Pl', 'HM-ES-PMSw1-Pl-DN-R1']):
          yield Service(item=line)


def check_homematic_power(item, section):
  for line in section:
    if line == item:
      data = section[line]
      current = round(float(data.get('CURRENT', ['0'])[0])/1000,3)
      energy_counter = round(float(data.get('ENERGY_COUNTER', ['0'])[0]),1)
      frequency = round(float(data.get('FREQUENCY', ['0'])[0]),2)
      power = round(float(data.get('POWER', ['0'])[0]),2)
      voltage = round(float(data.get('VOLTAGE', ['0'])[0]),1)
      last_change = data.get('ENERGY_COUNTER', ['-', '-'])[1]

      yield Result(state=State.OK, summary= f'Current: {current:.3f}')
      yield Result(state=State.OK, summary= f'Energy: {energy_counter/1000:.3f}kWh')
      yield Result(state=State.OK, summary= f'Frequency: {frequency:.2f}Hz')
      yield Result(state=State.OK, summary= f'Power: {power:.2f}W')
      yield Result(state=State.OK, summary= f'Voltage: {voltage:.1f}V')
      yield Result(state=State.OK, summary= f'last change: {last_change}')

      yield Metric("current", current)
      yield Metric("energy", energy_counter)
      yield Metric("frequency", frequency)
      yield Metric("power", power)
      yield Metric("voltage", voltage)


register.check_plugin(
    name = "homematic_power",
    service_name = "HM-Power %s",
    sections=["homematic"],
    discovery_function = discover_homematic_power,
    check_function = check_homematic_power,
)



# check for dutycycle
def discover_homematic_dutycycle(section):
  for line in section:
    data = section[line]
    if isinstance(data, dict) and data.get('DUTY_CYCLE', None) is not None:
      yield Service(item=line)


def check_homematic_dutycycle(item, params, section):
  (warn, crit) = params['dutycycle']
  for line in section:
    if line == item:
      data = section[line]
      dutycycle = data.get('DUTY_CYCLE', None)

      if dutycycle is None :
        yield Result(state=State.CRIT, summary= f"Duty Cycle is not available")

      dutycycle = int(dutycycle[0])

      if dutycycle >= crit:
        yield Result(state=State.CRIT, summary= f'Duty Cycle: {dutycycle}% (warn/crit: {warn}/{crit})')
      elif dutycycle >= warn:
        yield Result(state=State.WARN, summary= f'Duty Cycle: {dutycycle}% (warn/crit: {warn}/{crit})')
      else:
        yield Result(state=State.OK, summary= f'Duty Cycle: {dutycycle}%')

      yield Metric("dutycycle", dutycycle, levels=(warn, crit), boundaries=(0,100))


register.check_plugin(
    name = "homematic_dutycycle",
    service_name = "Homematic Duty Cycle %s",
    sections=["homematic"],
    discovery_function = discover_homematic_dutycycle,
    check_function = check_homematic_dutycycle,
    check_default_parameters={'dutycycle' : (50,70)},
    check_ruleset_name="homematic_dutycycle",)



# Check for door & window sensor
def discover_homematic_sec(section):
    for line in section:
        data = section[line]
        if isinstance(data, dict):
            HssType = data.get('HSSTYPE', None)

            if HssType is not None:
                if set(HssType).issubset(['HM-Sec-SCo', 'HM-Sec-SC-2']):
                    yield Service(item=line)


def check_homematic_sec(item, section):
    for line in section:
        if line == item:
            data = section[line]
            if data.get('STATE', False):
                state = data.get('STATE')
                yield Result(state=State.OK, summary= f'Window or door is open')
                if len(data.get('STATE'))==2:
                    yield Result(state=State.OK, summary= f'last change: %s' % data.get('STATE')[1])
                yield Metric("door", 1)
            else:
                yield Result(state=State.OK, summary= f'Window or door is closed')
                yield Result(state=State.OK, summary= f'last change: -')
                yield Metric("door", 0)


register.check_plugin(
    name = "homematic_sec",
    service_name = "HM-Sec %s",
    sections=["homematic"],
    discovery_function = discover_homematic_sec,
    check_function = check_homematic_sec,
)

