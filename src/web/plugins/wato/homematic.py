#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  _   _       _  __ _ 
# | | | |     (_)/ _(_)
# | | | |_ __  _| |_ _ 
# | | | | '_ \| |  _| |
# | |_| | | | | | | | |
#  \___/|_| |_|_|_| |_|
#
# created: 01/2022
#
# Author: Frank Baier
#
from cmk.gui.i18n import _
from cmk.gui.valuespec import (
    Dictionary,
    Integer,
    Tuple,
)
from cmk.gui.plugins.wato import (
    RulespecGroupCheckParametersNetworking,
    RulespecGroupCheckParametersEnvironment,
    CheckParameterRulespecWithoutItem,
    CheckParameterRulespecWithItem,
    rulespec_registry,
)


def _parameter_valuespec_homematic_climate():
    return Dictionary(elements=[
        ( "humidity_low",
        Tuple(
            title = _("Low Humidity Level"),
            elements = [
                Integer(title = _("warning at"), unit=u"%", default_value=35),
                Integer(title = _("critical at"), unit=u"%", default_value=30),
            ],
        )),
        ( "humidity_high",
        Tuple(
            title = _("High Humidity Level"),
            elements = [
                Integer(title=_("warning at"), unit=u"%", default_value=60),
                Integer(title=_("critical at"), unit=u"%", default_value=70),
            ],
        )),
    ],)

def _item_valuespec_homematic_climate():
    return TextAscii(title=_("Sensor"))

rulespec_registry.register(
    CheckParameterRulespecWithItem(
        check_group_name="homematic_climate",
        group=RulespecGroupCheckParametersEnvironment,
        match_type="dict",
        parameter_valuespec=_parameter_valuespec_homematic_climate,
        item_spec=_item_valuespec_homematic_climate,
        title=lambda: _("Humidity Levels for Homematic"),
    ))




def _parameter_valuespec_homematic_dutycycle():
    return Dictionary(elements=[
        ( "dutycycle",
            Tuple(
            title = _("Duty Cycle"),
            elements = [
                Percentage(title = _("warning at"), default_value=50),
                Percentage(title = _("critical at"), default_value=70),
            ],
        )),
    ],)


rulespec_registry.register(
    CheckParameterRulespecWithoutItem(
        check_group_name="homematic_dutycycle",
        group=RulespecGroupCheckParametersNetworking,
        match_type="dict",
        parameter_valuespec=_parameter_valuespec_homematic_dutycycle,
        title=lambda: _("Homematic Radio Dutycycle"),
    ))
