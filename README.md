# Homematic Checkmk addon

This project contains an addon for the Homematic CCU2/CCU3 or a Raspberrymatic device which acts as an check_mk_agent.

The Plugin comes from https://github.com/alexreinert/homematic_check_mk/
The check was rewritten so support more devices and data and run only on Checkmk 2.0 and above. It uses the new Checkmk API!

At the moment there are some plugins for the check_mk server implemented:

- Shows basic information about the CCU3 device (Type/Serial/...) (on CCU3, "CCU2" will be shown, this is a bug on the device)
- Only channels in Gewerk "Monitored" will be shown in Checkmk
- More easy to expand for new devices
- Check for devices with low batteries (All devices)
- Check for unreachable devices (All devices)
- Check for all other service messages (All devices)
- Check for dutycycle (CCU and all connected LAN GW)
- Check for humidity with configurable limits
- Check for temperture
- Check for power-plug adapter to show voltage/ampere/power/....

Don't hesitate to ask for additional check plugins.


# Bugs / Important stuff

- CCU3 will be reported as CCU2 (this is a CCU Firmware Problem)
- The CCU Plugin is manually added to the Check (MKP) with a static ZIP-file.


# Building

To build all the plugins, clone [this repository](https://github.com/mbirth/check_mk-plugins/)
and run:

    make all

(Make sure to have make and Python3 installed.)

You can also build a single plugin, e.g.:

    make 

The files (*.mkp) will be written into the `build/` directory.


# Installing

SSH into your Checkmk server, change to the user of the site you want to install the plugin,
then run:

    mkp install /path/to/plugin.mkp

or use WATO to install.

and

CCU Addon: Use the normal addon installation process of the CCU/Raspberrymatic


# Thanks

to

- Markus Birth for the Build environemnt
  https://github.com/mbirth/check_mk-plugins
- Alexander Reinert for the Homematic Plugin
  https://github.com/alexreinert/homematic_check_mk


# License
Apache 2
